import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import config from "config";
import { execFile } from "child-process";

import { messagesRouter } from "./routes/messages/mesaages.routes.js";
// import { restartRouter } from "./routes/restart/restart.routes.js";

const app = express();
const PORT = config.get("port") || 5000;
const MONGO_DB_URL = config.get("mongoDbUrl");

app.use(cors({ origin: "*" }));
app.use(express.json({ extended: true }));

app.use("/api/mes", messagesRouter);
// app.use("/api/restart", restartRouter);
app.get("/api/restart", async (req, res) => {
  try {
    cp.exec(`start "" "\\\\${req.params.name}"`);
    console.log(req.params.name);
  } catch (e) {
    console.log(e.message);
  }
});

async function start() {
  try {
    await mongoose.connect(MONGO_DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => {
      console.log(`Server start on ${PORT}`);
    });
  } catch (e) {
    console.log(e.message);
  }
}
start();
