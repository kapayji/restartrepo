import express from "express";
import cors from "cors";
const app = express();
const PORT = process.env.PORT || 5000;

import { restartRouter } from "./routes/restart/restart.routes.js";

app.use(cors());
app.use(express.json({ extended: true }));

app.use("/api/restart", restartRouter);

async function start() {
  try {
    app.listen(PORT, () => {
      console.log(`Server start on ${PORT}`);
    });
  } catch (e) {
    console.log(e.message);
  }
}
start();
