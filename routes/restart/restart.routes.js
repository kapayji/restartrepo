import Router from "express";
import { restartServer } from "../../controllers/restart/index.js";

export const restartRouter = Router();

restartRouter.get("/restartserver", async (req, res) => {
  restartServer(req, res);
});
